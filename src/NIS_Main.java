import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * �Main class of NIC Compresser/Decompresser
 */
public class NIS_Main {
    private static String decompMode = null;
    private static String inputFileName = null;
    private static String outputFileName = null;
    public static boolean skipConsole = false;
    /**
     * Main class of the program, determines if startup parameters are used and if not,
     * it asks user if he wants to compress or decompress and launches given part of the program
     *
     * @param args Startup arguments
     */
    public static void main(String[] args) {
        proccessArgs(args);

        if (decompMode == null) {
            System.out.println("Zadejte, zda chce komprimovat (C), nebo dekomprimovat (D). [C/D]: ");
            Scanner sc = new Scanner(System.in);
            while (sc.hasNextLine()) {
                String input = sc.nextLine();
                if (input.toLowerCase().equals("d")) {
                    decompMode = "D";
                } else if (input.toLowerCase().equals("c")) {
                    decompMode = "C";
                }
            }
        }

        if (decompMode.equals("D")) {
            new Decompresser(inputFileName, outputFileName);
        } else {
            new Compresser(inputFileName, outputFileName);
        }
    }

    /**
     * Processes arguments and fills variables
     * @param args startup arguments
     */
    private static void proccessArgs(String[] args) {
        boolean nextInput = false;
        boolean nextOutput = false;

        for (String arg : args) {
            if (arg.toLowerCase().equals("-d")) {
                decompMode = "D";
            } else if (arg.toLowerCase().equals("-c")) {
                decompMode = "C";
            } else if (arg.toLowerCase().equals("-i")) {
                nextInput = true;
            } else if (arg.toLowerCase().equals("-o")) {
                nextOutput = true;
            } else if (arg.toLowerCase().equals("-s")) {
                skipConsole = true;
            } else if (nextInput) {
                inputFileName = arg;
                nextInput = false;
            } else if (nextOutput) {
                outputFileName = arg;
                nextOutput = false;
            } else if (arg.toLowerCase().contains("-h")) {
                StringBuilder sb = new StringBuilder();
                sb.append("Program simuluj�c� kompresi a dekompresi pomoc� nov�ho NIS algoritmu.\n");
                sb.append('\n');
                sb.append("P�ep�na�e:\n");
                sb.append("-c -> automaticky zapne kompresn� m�d\n");
                sb.append("-d -> automaticky zapne dekompresn� m�d\n");
                sb.append("-i filename -> na�te vstupn� data ze souboru\n");
                sb.append("-o filename -> provede v�stup do konzole a z�rove� do zadan�ho souboru\n");
                sb.append("-s -> Vyp�e do konzole pouze fin�ln� statistiky.\n");
                sb.append('\n');
                sb.append("P��klady:\n");
                sb.append("java -jar NIS.jar\n");
                sb.append(" - Spust� program, ve�ker� data je nutn� zadat do konzole\n");
                sb.append("java -jar NIS.jar -d\n");
                sb.append(" - Spust� program v dekompresn�m re�imu, vstupn� data je nutn� zadat do konzole\n");
                sb.append("java -jar NIS.jar -c -i vstup.txt -o vystup.txt\n");
                sb.append(" - Provede kompresi vstupn�ho souboru vstup.txt a v�sledek ulo�� do vystup.txt\n");

                System.out.print(sb.toString());
                System.exit(0);
            }
        }
    }
}
