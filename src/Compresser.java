import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class that contains methods used for compressing data
 */
public class Compresser {
    Scanner sc;
    private final int ASCII_WORD_LENGTH = 7;
    private PrintStream fileOutStream;
    private Statistics st;

    /**
     * Constructor of the class. If filepaths are given in the arguments,
     * it tries to parse the input file and create the output filestream,
     * if not it reads the data from the console.
     *
     * @param inputFileName Filename of input file
     * @param outputFileName Filename of output file
     */
    public Compresser(String inputFileName, String outputFileName) {
        st = new Statistics();
        st.initBitCounting();
        sc = new Scanner(System.in);
        if (outputFileName != null) {
            try {
                fileOutStream = new PrintStream(new File(outputFileName));
            } catch (FileNotFoundException e) {
                System.out.println("V�stupn� soubor nebyl nalezen. " + outputFileName);
            }
        }

        if (inputFileName != null) {
            File f = new File(inputFileName);
            if (f.exists()) {
                parseFile(f);
            } else {
                System.out.println("File not found! Given path: " + inputFileName);
            }
        } else {
            readConsole();
        }
    }

    /**
     * Asks user if he wants to compress ASCII letters
     * or bit values and calls funtions used for decoding.
     */
    private void readConsole() {
        System.out.println("Zadejte, zda budete k�dovat ASCII znaky (A), nebo bity (B) [A/B]:");
        while (sc.hasNextLine()) {
            String input = sc.nextLine();
            if (input.toLowerCase().equals("a")) {
                readASCIIFromConsole();
                break;
            } else if (input.toLowerCase().equals("b")) {
                readBitsFromConsole();
                break;
            }
        }
    }

    /**
     * Reads bits from console input and calls compress function.
     * After compression it outputs statistical data.
     */
    private void readBitsFromConsole() {
        System.out.println("Zadejte bity (0 a 1) jednoho datov�ho slova, kter� chcete zak�dovat.");
        String input = sc.nextLine();

        Tools.printString("B;" + input.length() + "\n", fileOutStream);
        st.startTimer();
        compressBits(input);
        st.stopTimer();
        System.out.println(st.toString());
    }

    /**
     * Reads ASCII letters from console input and calls compress function.
     * After compression it outputs statistical data.
     */
    private void readASCIIFromConsole() {
        System.out.println("Zadejte text ke komprimaci. Ka�d� znak bude komprimov�n samostatn�.");
        String input = sc.nextLine();

        Tools.printString("A\n", fileOutStream);
        st.startTimer();
        compressASCII(input);
        st.stopTimer();
        System.out.println(st.toString());
    }

    /**
     * Converts all chars into bits, stores them into List of BitArray and prints them out.
     * It also handles storing statistical data.
     * @param ascii ASCII string to compress
     */
    private void compressASCII(String ascii) {
        for (char c : ascii.toCharArray()) {
            st.addOriginalBits(ASCII_WORD_LENGTH);
            BitArray ba = BitArray.fromInt(c, ASCII_WORD_LENGTH);
            ArrayList<BitArray> compressedBitSets = compressBitSet(ba);

            st.addAllFinalBits(compressedBitSets);
            Tools.outputCompressedBitSet(compressedBitSets, fileOutStream);
        }
    }

    /**
     * Converts string containing bits to BitArray and calls compressing function.
     * After that it outputs the compressed data. It also handles storing statistical
     * data.
     * @param bitString
     */
    private void compressBits(String bitString) {
        BitArray bits = BitArray.fromString(bitString);
        st.addOriginalBits(bits.length());

        ArrayList<BitArray> compressedBitSets = compressBitSet(bits);
        st.addAllFinalBits(compressedBitSets);
        Tools.outputCompressedBitSet(compressedBitSets, fileOutStream);
    }

    /**
     * First this function determines from the first line if file contains
     * ASCII or Binary data and then calls appropriate compressing functions.
     * After compressing it outputs statistical data
     * @param f File pointer
     */
    private void parseFile(File f) {
        boolean outputHeader = false;
        st.startTimer();
        try {
            Scanner scFile = new Scanner(f);

            String mode = null;
            while (scFile.hasNextLine()) {
                String data = scFile.nextLine();
                if (mode == null) {
                    if (data.equals("A")) {
                        mode = "A";
                    } else if (data.equals("B")) {
                        mode = "B";
                    } else {
                        System.out.println("Unknown mode: " + mode);
                    }
                } else {
                    if (mode.equals("A")) {
                        if (!outputHeader) {
                            Tools.printString("A\n", fileOutStream);
                            outputHeader = true;
                        }

                        compressASCII(data);
                    } else {
                        if (!outputHeader) {
                            Tools.printString("B;" + data.length() + "\n", fileOutStream);
                            outputHeader = true;
                        }

                        compressBits(data);
                    }
                    Tools.printChar('\n', fileOutStream);
                }
            }
            st.stopTimer();
            System.out.println(st.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Does both compression passes and returns pass with better result.
     * @param bs BitArray containing data to compress
     * @return List of BitArrays with compressed data
     */
    private ArrayList<BitArray> compressBitSet(BitArray bs) {
        ArrayList<BitArray> passBy0 = doCompressionPass(false, bs);
        ArrayList<BitArray> passBy1 = doCompressionPass(true, bs);

        return comparePasses(passBy0, passBy1);
    }

    /**
     * Loops through both passes, making sum of bits in all BitArrays. After that it returns BitArray with less bits.
     * @param arrayList1 First ArrayList of BitArrays to compare
     * @param arrayList2 Second ArrayList of BitArrays to compare
     * @return returns ArraysList with smaller sum of bits.
     */
    private ArrayList<BitArray> comparePasses(ArrayList<BitArray> arrayList1, ArrayList<BitArray> arrayList2) {
        int sizePass0 = 0;
        for (BitArray bs : arrayList1) {
            sizePass0 += bs.length();
        }

        int sizePass1 = 0;
        for (BitArray bs : arrayList2) {
            sizePass1 += bs.length();
        }

        return (sizePass0 < sizePass1 ? arrayList1 : arrayList2);
    }

    /**
     * Makes a compression pass by 0 or 1.
     * @param passType Determines if compression pass is by 0 or 1
     * @param bitsToCompress BitArray of data.
     * @return Returns ArrayList of BitArrays containing compressed data
     */
    private ArrayList<BitArray> doCompressionPass(boolean passType, BitArray bitsToCompress) {
        ArrayList<BitArray> pass = new ArrayList<>();
        BitArray controlBits = new BitArray();
        controlBits.set(0, bitsToCompress.get(0));
        controlBits.set(1, passType);
        pass.add(controlBits);

        int vzdalenost = 0;
        for (int i = 1; i<bitsToCompress.length(); i++) {
            if(bitsToCompress.get(i) == passType) {
                pass.add(BitArray.fromInt(vzdalenost));
                vzdalenost = -1;
            }

            vzdalenost++;
        }

        return pass;
    }
}