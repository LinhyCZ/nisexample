import java.util.HashMap;

/**
 * Structure that represents array of bits. Bits are "get" and "set" with boolean values with false representing 0 and true representing 1.
 */
public class BitArray {
    private final HashMap<Integer, Boolean> values;
    private int length = -1;

    /**
     * Constructor
     */
    public BitArray() {
        values = new HashMap<>();
    }

    /**
     * Sets bit at given index to given value
     * @param index bit index
     * @param value bit value
     */
    public void set(int index, boolean value) {
        values.put(index, value);
        length = Math.max(index, length);
    }

    /**
     * Appends a bit of given value to the end of array
     * @param value Value of bit
     */
    public void add(boolean value) {
        length++;
        values.put(length, value);
    }

    /**
     * Returns value of bit at given index
     * @param index Index of bit
     * @return Value of bit
     */
    public boolean get(int index) {
        return values.getOrDefault(index, false);
    }

    /**
     * Returns number of bits stored in array
     * @return array length
     */
    public int length() {
        return length + 1;
    }

    /**
     * Converts BitArray to corresponding integer value
     * @return integer value of this BitArray
     */
    public int toInt() {
        String bits = this.toString();
        if (!bits.equals("")) {
            return Integer.parseInt(bits, 2);
        }
        return Integer.MIN_VALUE;
    }

    @Override
    /**
     * Converts BitArray to a String containing all bit values
     * @return String with bits (e.g. 00110011)
     */
    public String toString() {
        int length = this.length();
        String s = "";

        for (int i = 0; i < length; i++) {
            if (this.get(i)) {
                s += "1";
            } else {
                s += "0";
            }
        }

        return s;
    }

    /* STATIC METHODS */

    /**
     * Converts String representation of bits to a BitArray
     * @param bitString String with bits (e.g. 00110011)
     * @return BitArray with bits at given places
     */
    public static BitArray fromString(String bitString) {
        char[] chars = bitString.trim().toCharArray();
        BitArray bits = new BitArray();

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '0') {
                bits.set(i, false);
            } else if (chars[i] == '1') {
                bits.set(i, true);
            } else {
                System.out.println("Illegal character: " + chars[i]);
            }
        }

        return bits;
    }

    /**
     * Converts integer value to a bit array, appending 0 at the start to match given length
     * @param value integer value to convert
     * @param bitArrayLength Smallest count of bits in the array
     * @return BitArray with bits of given integer
     */
    public static BitArray fromInt(int value, int bitArrayLength) {
        String bits = Integer.toBinaryString(value);
        BitArray bitArray = new BitArray();
        int l = bits.length();

        for (int i = bits.length(); i < bitArrayLength; i++) {
            bitArray.add(false);
        }

        for (char bitChar : bits.toCharArray()) {
            bitArray.add((bitChar == '1' ? true : false));
        }

        return bitArray;
    }

    /**
     * Coverts int to BitArray
     * @param value integer value to convert
     * @return BitArray with bits of given integer
     */
    public static BitArray fromInt(int value) {
        return BitArray.fromInt(value, -1);
    }
}
