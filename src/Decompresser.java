import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class that contains methods used for decompressing data
 */
public class Decompresser {
    Scanner sc;
    Statistics st = new Statistics();
    private final int ASCII_WORD_LENGTH = 7;
    private PrintStream fileOutStream;

    /**
     * Constructor of the class. If filepaths are given in the arguments,
     * it tries to parse the input file and create the output filestream,
     * if not it reads the data from the console.
     *
     * @param inputFileName Filename of input file
     * @param outputFileName Filename of output file
     */
    public Decompresser(String inputFileName, String outputFileName) {
        sc = new Scanner(System.in);
        if (outputFileName != null) {
            try {
                fileOutStream = new PrintStream(new File(outputFileName));
            } catch (FileNotFoundException e) {
                System.out.println("V�stupn� soubor nebyl nalezen. " + outputFileName);
            }
        }

        if (inputFileName != null) {
            File f = new File(inputFileName);
            if (f.exists()) {
                parseFile(f);
            } else {
                System.out.println("File not found! Given path: " + inputFileName);
            }
        } else {
            readConsole();
        }
    }
    /**
     * First this function determines from the first line if file contains
     * ASCII or Binary data and length of the binary word and then calls
     * appropriate compressing functions.
     *
     * @param f File pointer
     */
    private void parseFile(File f) {
        try {
            st.startTimer();
            Scanner scFile = new Scanner(f);

            String mode = null;
            int wordLength = 0;
            while (scFile.hasNextLine()) {
                String data = scFile.nextLine();
                if (mode == null) {
                    if (data.equals("A")) {
                        mode = "A";
                    } else if (data.contains("B")) {
                        mode = "B";
                        String[] splitData = data.split(";");
                        if (splitData.length == 2) {
                            wordLength = Integer.parseInt(splitData[1]); //Test parsing
                        } else {
                            System.out.println("Word length not specified");
                        }
                    } else {
                        System.out.println("Unknown mode: " + mode);
                        return;
                    }
                } else {
                    if (data.equals("")) {
                        Tools.printChar('\n', fileOutStream);
                        continue;
                    }

                    if (mode.equals("A")) {
                        Tools.printChar(decompressASCII(data), fileOutStream);
                    } else {
                        Tools.printString(decompressBits(data, wordLength).toString(), fileOutStream);
                    }
                }
            }
            Tools.printChar('\n', fileOutStream);
            st.stopTimer();
            System.out.println(st.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads length of compressed word and compressed word itself from user input
     * and calls decompressing functions
     */
    private void readBitsFromConsole() {
        System.out.println("Zadejte d�lku dek�dovan�ho datov�ho slova:");
        String input = sc.nextLine();
        int wordLength = Integer.parseInt(input);

        System.out.println("Zadejte zak�dovan� datov� slovo.");
        input = sc.nextLine();

        st.startTimer();
        decompressBits(input, wordLength);
        st.stopTimer();
        System.out.println(st.toString());
    }

    /**
     * Reads compressed ascii char from user input
     * and calls decompressing functions
     */
    private void readASCIIFromConsole() {
        System.out.println("Zadejte zak�dovan� datov� slovo.");
        String input = sc.nextLine();

        st.startTimer();
        Tools.printChar(decompressASCII(input), fileOutStream);
        st.stopTimer();
        System.out.println(st.toString());
    }

    /**
     * Initial read from the console, determining if user is
     * going to input binary or ASCII data
     */
    private void readConsole() {
        System.out.println("Zadejte, zda budete dek�dovat ASCII znaky (A), nebo bity (B) [A/B]:");
        while (sc.hasNextLine()) {
            String input = sc.nextLine();
            if (input.toLowerCase().equals("a")) {
                readASCIIFromConsole();
                break;
            } else if (input.toLowerCase().equals("b")) {
                readBitsFromConsole();
                break;
            }
        }
    }

    /**
     * Calls decompressBits function with constant defining length of the ASCII char
     * (7 bits). After that converts decompressed bits into integer and returns
     * corresponding char.
     *
     * @param data data containing ASCII char.
     */
    private char decompressASCII(String data) {
        BitArray decompressedBits = decompressBits(data, ASCII_WORD_LENGTH);
        int charNumber = decompressedBits.toInt();

        return (char) charNumber;
    }

    /**
     * Splits given compressed data into a set of control bits and into an List of BitArrays
     * containing distance for each bit. Then calls decompress function with List containing
     * all bits.
     *
     * @param data String containing compressed data
     * @param wordLength Length of output binary word
     * @return BitArray containing bits of decompressed word
     */
    private BitArray decompressBits(String data, int wordLength) {
        String[] split = data.split("-");
        String controlBitsString = split[0];

        List<BitArray> compressedBitArrays = new ArrayList<>();
        compressedBitArrays.add(BitArray.fromString(controlBitsString));

        if (split.length != 1) {
            split = split[1].split(",");

            for (String bitString : split) {
                compressedBitArrays.add(BitArray.fromString(bitString));
            }
        }

        return decompressWord(compressedBitArrays, wordLength);
    }

    /**
     * Function that decompresses given List of BitArrays according to NIS algorithm.
     * @param compressedBitArrays input List of BitArrays containing one word.
     * @param wordLength length of output word.
     * @return BitArray containing bits of decompressed word
     */
    private BitArray decompressWord(List<BitArray> compressedBitArrays, int wordLength) {
        BitArray decompressed = new BitArray();
        decompressed.add(compressedBitArrays.get(0).get(0));

        int indexFromReferentionBit = 0;
        int nextBit = (compressedBitArrays.size() != 1 ? compressedBitArrays.get(1).toInt() : Integer.MAX_VALUE);
        int numberOfDecompressedArrays = 1;

        for (int i = 1; i < wordLength; i++) {
            if (indexFromReferentionBit == nextBit) {
                if (compressedBitArrays.get(0).get(1) == false) {
                    decompressed.set(i, false);
                } else {
                    decompressed.set(i, true);
                }

                numberOfDecompressedArrays += 1;
                indexFromReferentionBit = -1;
                if (numberOfDecompressedArrays == compressedBitArrays.size()) {
                    nextBit = Integer.MAX_VALUE;
                } else {
                    nextBit = compressedBitArrays.get(numberOfDecompressedArrays).toInt();
                }
            } else {
                if (compressedBitArrays.get(0).get(1) == false) {
                    decompressed.set(i, true);
                } else {
                    decompressed.set(i, false);
                }
            }
            indexFromReferentionBit++;
        }

        return decompressed;
    }
}