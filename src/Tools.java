import java.io.PrintStream;
import java.util.ArrayList;

/**
 * Static class with tools used by other parts of the program.
 */
public class Tools {
    /**
     * Prints out BitArrays after compression. If other PrintStream is specified, it prints into this stream as well.
     * @param compressed List of BitArrays containing compressed bits
     * @param fileOutStream Output stream of file to output
     */
    public static void outputCompressedBitSet(ArrayList<BitArray> compressed, PrintStream fileOutStream) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < compressed.size(); i++) {
            sb.append(compressed.get(i).toString());
            if (i == 0) {
                sb.append(" - ");
            } else if (i != compressed.size() - 1) {
                sb.append(", ");
            }
        }
        sb.append('\n');
        if (NIS_Main.skipConsole == false) {
            System.out.print(sb.toString());
        }
        if (fileOutStream != null) {
            fileOutStream.print(sb.toString());
        }
    }

    /**
     * Prints out char. If other PrintStream is specified, it prints into this stream as well.
     * @param c char to print
     * @param fileOutStream Output stream of file to output
     */
    public static void printChar(char c, PrintStream fileOutStream) {
        if (NIS_Main.skipConsole == false) {
            System.out.print(c);
        }
        if (fileOutStream != null) {
            fileOutStream.print(c);
        }
    }

    /**
     * Prints out string. If other PrintStream is specified, it prints into this stream as well.
     * @param s string to print
     * @param fileOutStream Output stream of file to output
     */
    public static void printString(String s, PrintStream fileOutStream) {
        if (NIS_Main.skipConsole == false) {
            System.out.print(s);
        }
        if (fileOutStream != null) {
            fileOutStream.print(s);
        }
    }
}
