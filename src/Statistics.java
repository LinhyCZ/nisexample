import java.util.ArrayList;

public class Statistics {
    private int originalBits = Integer.MIN_VALUE;
    private int finalBits = Integer.MIN_VALUE;

    private long startNanos;
    private long outputNanos;

    /**
     * Constructor of statistics class
     */
    public Statistics() {
        //Empty constructor
    }

    /**
     * Sets initial values to zero.
     */
    public void initBitCounting() {
        originalBits = 0;
        finalBits = 0;
    }

    /**
     * Add bits to the counter of bits of original message
     * @param bits number of bits to add
     */
    public void addOriginalBits(int bits) {
        originalBits += bits;
    }

    /**
     * Add bits to the counter of bits of compressed message
     * @param bits number of bits to add
     */
    public void addFinalBits(int bits) {
        finalBits += bits;
    }

    /**
     * Counts all bits in the ArrayList of BitArrays and adds them to the
     * counter of compressed bits
     * @param list list with bits to count
     */
    public void addAllFinalBits(ArrayList<BitArray> list) {
        for (BitArray ba : list) {
            addFinalBits(ba.length());
        }
    }

    /**
     * Starts execution time counter
     */
    public void startTimer() {
        this.startNanos = System.nanoTime();
    }

    /**
     * Stops execution time counter
     */
    public void stopTimer() {
        this.outputNanos = System.nanoTime() - this.startNanos;
    }

    /**
     * Outputs time used for script execution
     * @return time used for script execution in nanos
     */
    private long getNanos() {
        return outputNanos;
    }

    @Override
    /**
     * Returns simple statistics table with collected data.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('\n');
        sb.append("=========================================\n");
        sb.append('\n');
        sb.append("Final statistics\n");
        sb.append('\n');
        sb.append("=========================================\n");
        if (finalBits != Integer.MIN_VALUE) {
            sb.append("Original size: " + originalBits + " bits\n");
            sb.append("Compressed size: " + finalBits + " bits\n");
            sb.append("=========================================\n");
            sb.append("Saved bits: " + (originalBits - finalBits) + "\n");
            double ratio = Math.round(((double) finalBits / originalBits) * 10000) / 100.0;
            sb.append("Compression ratio: " + ratio + "%\n");
            sb.append("=========================================\n");
        }
        sb.append("Time used for compression: " + this.getNanos() + " ns\n");
        sb.append("Time used for compression: " + (this.getNanos() / 1000000) + " ms\n");
        sb.append("=========================================\n");
        return sb.toString();
    }
}
